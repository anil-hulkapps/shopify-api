<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/getProduct',[\App\Http\Controllers\APIController::class,'getProduct']);
Route::get('/createProduct',[\App\Http\Controllers\APIController::class,'createProduct']);
Route::get('/updateProduct',[\App\Http\Controllers\APIController::class,'updateProduct']);
Route::get('/deleteProduct',[\App\Http\Controllers\APIController::class,'deleteProduct']);
