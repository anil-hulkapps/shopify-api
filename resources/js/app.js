require('./bootstrap');
import Vue from 'vue/dist/vue.js';

let app = new Vue({
    el:'#app',
    data:{
        name:"HulkApps",
        isOk:true,
        items: ["shoes","goggles"],
        newItem: '',
        sets: [[ 1, 2, 3, 4, 5 ], [6, 7, 8, 9, 10]],
        age:25,
        x:0,
        y:0,
        available:true,
        width:100,
    },
    methods:{
        addItem(){
            this.items.push(this.newItem)
        },

        even: function (numbers) {
            return numbers.filter(function (number) {
                return number % 2 === 0
                })
            },
        test(){
            return "This is Test(To print Method)";
        },
        coordinate:function (event){
            this.x = event.offsetX;
            this.y = event.offsetY;
        },
        google:function (){
            alert("This is prevent modifier!")
        },
        keyboard:function (){
            console.log("You have entered name!")
        },
        changeWidth:function (){
            this.width -=10;
        }

    },
    computed:{
      reversedName(){
         return  this.name.split('').reverse().join('')
      }
    },
    template : `
      <div id="firstContainer">
              <div>
                  <h1>Reverse Name</h1>
                  <input v-model="name">
                  <!--        <button @click="changeName">Click</button>-->
                  <p>{{reversedName}}</p>
                  <p v-show="isOk">everything is ok</p>
                  <button @click="isOk=!isOk">Not ok</button>
              </div><br><br>
              <hr>
              <div>
                  <h1>To-do List</h1>
                  <input type="text" v-model="newItem">
                  <button @click="addItem">Add</button>
                  <ul>
                      <li v-for="(item,index) in items">{{index+1}}->{{item}}</li>
                  </ul>
              </div>
              <hr>
              <div>
                  <h1>Even Numbers</h1>
                  <ul v-for="set in sets">
                      <li v-for="n in even(set)">{{ n }}</li>
                  </ul>
                  <h2>{{test()}}</h2>
              </div>
              <hr>
              <div>
                  <h3>Your age is {{age}}</h3>
                  <button @click.once="age++">Increment</button>
                  <button @click="age--">Decrement</button>
                  <div class="coordinate" @mousemove="coordinate"
                       @click="available=!available"
                       v-bind:class={green:available}>
                      <h3>Move mouse in Div to see coordinates</h3>
                      <p>{{x}} , {{y}}</p>
                  </div>
                  <a @click.prevent="google" href="https://www.google.com/">Click Here to get alert(prevent modifier)</a><br>
                  <hr><br>
                  <h2>Keyboard event(Alt+Ctrl to console)</h2>
                  <label>Enter name</label>
                  <input @keyup.alt.enter="keyboard">
                  <br><br>
                  <hr>
              </div>
              <h2>Simple Game(Style binding)</h2>
              <div class="parent">
                  <div class="child" :style="{width:width+'%'}"></div>
              </div><br>
              <button @click="changeWidth">Eat It</button>
              <button @click="width=100">Reset</button>
      </div>


    `
});

const app2= new Vue({
    el:'#app2',
    data:{
        title: 'This is second instance'
    },
    computed:{
      name(){
          return app.name
      }
    },
    template:`
    <div id="secondContainer">
    <h1>{{title}}-{{name}}</h1>
    </div>
    `
});
