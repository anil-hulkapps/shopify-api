<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class APIController extends Controller
{
    function getProduct(){
        $query = '{
              shop {
                name
              }
              products(first: 3) {
                edges {
                  node {
                    id
                    title
                    totalInventory
                    totalVariants
                  }
                }
              }
            }
            ';

        $data = ['query'=>$query];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        dd($response->json());
    }

    function createProduct(){
        $query = 'mutation MyMutation($input: ProductInput!) {
                  productCreate(input: $input) {
                    product {
                    id
                    title
                     }
                    }
                  }
                   ';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'title' => 'New product 3',
                    'bodyHtml'=>'This is new product2'
                ]
            ]
        ];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        dd($response->json());
    }
    function updateProduct(){
        $query = 'mutation productUpdate($input: ProductInput!) {
                      productUpdate(input: $input) {
                        product {
                          id
                          title
                        }
                        userErrors {
                          field
                          message
                        }
                      }
                    } ';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'id'=>'gid://shopify/Product/6695211204773',
                    'title' => 'Updated title',
                    'bodyHtml'=>'Updated body'
                ]
            ]
        ];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        dd($response->json());
    }
    function deleteProduct(){
        $query = 'mutation MyMutation ($input: ProductDeleteInput!){
                  productDelete(input: $input) {
                    shop {
                      id
                    }
                  }
                }
                ';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'id'=>'gid://shopify/Product/6706050498725',
                ]
            ]
        ];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        dd($response->json());
    }


}
