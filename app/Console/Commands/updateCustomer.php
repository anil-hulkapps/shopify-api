<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class updateCustomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateCustomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $query = 'mutation MyMutation($input: CustomerInput!) {
                  customerUpdate(input: $input) {
                    customer {
                      firstName
                      lastName
                    }
                    userErrors {
                      field
                      message
                    }
                  }
                }
                ';
        $data = [
            "query" => $query,
            "variables" => [
                "input" => [
                    'id' => 'gid://shopify/Customer/5197723041957',
                    'firstName' => 'Anil',
                    'lastName' => 'Chudasama'
                ]
            ]
        ];

        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
            ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        dd($response->json());

        $customers = json_decode($customers->body())->customers;

        return 0;
    }
}
