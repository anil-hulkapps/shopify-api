<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class getProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getProduct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Products through API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $query = '{
  shop {
    id
    name
  }
  products(first: 3) {
    edges {
      node {
        title
        totalInventory
        totalVariants
      }
    }
  }
}
';

        $data = ['query'=>$query];
        $response = Http::withBasicAuth(env('SHOPIFY_API_KEY'),env('SHOPIFY_API_SECRET'))
                    ->post('https://demo-anil.myshopify.com/admin/api/2021-04/graphql.json',$data);
        dd($response->json());
        return 0;


    }
}
